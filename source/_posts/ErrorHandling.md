#  Why error handling is important with promises and how it has to be done.

### JS Promises

- Promise is a way to handle Asynchronous operations.
- A promise is an object that represents a result of an operation that will be returned at some point in the future.

Example :

```js
    const url = "url";
    let responseObject = fetch(url);
    console.log(responseObject); // Promise{ [[PromiseStatus]]:pending
    console.log("fetching done"); // fetching done
```
In the above example, the fetch() executes Asynchronously and it returns a promise.
A promise will be in any one of the three states:
- Pending: Neither fulfilled nor rejected
- Fulfilled: executed successfully
- Rejected: execution failed

### Pending State

When an asynchronous operation is started initially the operation is in Pending State.

### Fullfilled State

When a Promise object is Resolved, the result is a value.

```js
    const url = "url";
    let responseObject = fetch(url);

    responseObject.then((response) => {
    console.log(response); // Response{ … }
    });
```    
    
### Rejected State

Sometimes Asynchronous execution may fail in case of an error the corresponding promise becomes rejected. For instance, Fetching a resource can be failed for various reasons like:
- Wrong URL path.
- The server is busy and taking a long time to respond.
- Network failure.

### Importance of Error handling
In unhandled promise rejections, if Error is not handled, The entire program will be terminated.

Example: 1. Without Error Handling

```js
    let promise2 = new Promise((resolve, reject) => {
    resolve("hello")
    })
    let promise1 = new Promise((resolve, reject) => {
    throw "Promise1 rejected"
    })

    promise1
    .then()
    .then(() => promise2)     //it will not execute 
    .then(data => console.log(data)) 
    
```

The throw statement in promise1 throws an exception as "Promise1 rejected" which means the state of the promise1 is rejected. Here the  Execution of the current function will stop (the statements after the throw won't be executed), and control will be passed to the first catch block in the call stack. If no catch block exists, the program will be terminated. so here we will not get any output because the entire program is terminated.

Exaple2: Handling Errors in Promises with .catch():
```js
    let promise2 = new Promise((resolve, reject) => {
    resolve("hello")
    })
    let promise1 = new Promise((resolve, reject) => {
    throw "Promise1 rejected"
    })

    promise1
        .then()
        .catch(err => console.log(err))     // "Promise1 rejected"
        .then(() => promise2)
        .then(data => console.log(data))    //"hello"
        
        Expected Output:
        "Promise1 rejected"
        "hello"
```

The throw statement in promise1 throws an exception as "Promise1 rejected" which means the state of the promise1 is rejected. Here the  Execution of the current function will stop, and control will be passed to the first catch block in the call stack. the catch block will catch the error thrown by promise1 and promise2 will be executed.

### Error Handling in Promises:
As we discussed above we can handle the error using .catch, is one of the most useful methods for error handling in promises.

1.Using .then() Handler

For example:

        .then() syntax: promise.then(successCallback, failureCallback) 
```js
    const url = "wrongurl";
    let responseObject = fetch(url);
    responseObject.then((data) => {
        console.log(data);  // prints 'All things went well!'
    },
        (error) => {
        console.log(error); // prints Error object
    }
    );
```

 Here:
 - successCallback: Called when a promise is resolved. resolve(argument) argument will be passed to successCallback
- failureCallback: Called when a promise is rejected. reject(argument) argument will be passed to failureCallback.

As discussed above We use catch() for handling errors. It’s more readable than handling errors inside the failureCallback callback of the then() callback. 
For example:
```js 
    const url = "wrongurl";
    let responseObject = fetch(url);
    responseObject.then((data) => {
    console.log(data);  // prints 'All things went well!'
    )},
    .catch((error) => {
    console.log(error); // prints Error object
    });
```

In the above example .then() and .catch() are called promise chaining. Combining multiple .then() s and .catch() is called promise chaining.

Here .catch() is called the rejection handler. We can chain promises in multiple stages. When a promise is rejected, the closest rejection handler(.catch()) will be called. 

### Conclusions:
- On promise rejection it will be through an exception.
- Unhandled promise will leads to programe termination.
- .catch() handles promise rejections.
- .then() also catch errors if the second argument is which is the error handler.

